package com.surespan.digital.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Properties;

public class ProducerApp {

    public static void main(String[] args) {
        System.out.println("Started Producer");

        Properties props = new Properties();
        props.put("bootstrap.servers", Kafka.SERVER);
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        try {
            Producer<String, String> producer = new KafkaProducer<>(props);

            System.out.print("> ");

            long counter = 0;
            String line = null;
            while (!(line = br.readLine().trim()).equals("")) {
                counter += 1;
                producer.send(new ProducerRecord<String, String>(Kafka.TOPIC, counter + ": " + line));

                System.out.print("> ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
