package com.surespan.digital.kafka;

import com.google.common.collect.Lists;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class Consumer2App {

    public static void main(String[] args) {
        System.out.println("Started Consumer 2 - test-consumer-2");

        try {
            KafkaConsumer<String, String> consumer = Consumer.createConsumer("test-consumer-2");
            consumer.subscribe(Lists.newArrayList(Kafka.TOPIC));

            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(100);
                for (ConsumerRecord<String, String> record : records) {
                    System.out.println("< " + record.value());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
